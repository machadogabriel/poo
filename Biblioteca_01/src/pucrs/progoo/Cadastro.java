package pucrs.progoo;

import java.util.ArrayList;
import java.util.Comparator;

public class Cadastro {

	private ArrayList<Usuario> usuarios;

	public Cadastro() {
		this.usuarios = new ArrayList<Usuario>();
	}

	public boolean cadastrar(String codigo, String nome, String cpf) {
		this.usuarios.add(new Usuario(codigo, nome, cpf));
		return true;
	}

	public void listarTodos() {
		this.usuarios.forEach(System.out::println);
	}

	public Usuario buscarCodigo(String codigo) {
		for (Usuario usuario : this.usuarios) {
			if (usuario.getCodigo().equals(codigo)){
				return usuario;
			}
		}

		return null;
	}

	public Usuario buscarNome(String nome) {

		for (Usuario usuario : this.usuarios) {
			if (usuario.getNome().equals(nome)){
				return usuario;
			}
		}

		return null;
	}

	public void ordenaNome() {
		usuarios.sort(Comparator.comparing(Usuario::getNome));
	}

	public void ordenaCpf() {
		usuarios.sort(Comparator.comparing(Usuario::getCpf));
	}

	public void ordenaNomeCpf() {
		usuarios.sort(Comparator.comparing(Usuario::getNome).thenComparing(Usuario::getCpf));
	}
}
