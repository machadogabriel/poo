package pucrs.progoo;

import java.time.LocalDate;

public class Emprestimo {

	private Usuario usuario;
	private Livro livro;
	private LocalDate dataLimite;
	private LocalDate dataDevolucao;

	public Emprestimo(Usuario usuario, Livro livro, LocalDate dataLimite) {
		this.usuario = usuario;
		this.livro = livro;
		this.dataLimite = dataLimite;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public LocalDate getDataLimite() {
		return dataLimite;
	}

	public void setDataLimite(LocalDate dataLimite) {
		this.dataLimite = dataLimite;
	}

	public LocalDate getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(LocalDate dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	@Override
	public String toString() {
		return "Nome: " + this.usuario.getNome() + " livro: " + this.livro.getTitulo() +" Código: " + this.livro.getCodigo() +" Data limite: " + this.dataLimite + " Data Devolução: " + this.dataDevolucao;
	}

	public void finalizar(LocalDate dataDev) {
		this.setDataDevolucao(dataDev);
	}
}
