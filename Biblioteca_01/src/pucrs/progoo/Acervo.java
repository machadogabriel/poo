package pucrs.progoo;

import java.util.ArrayList;
import java.util.Comparator;

public class Acervo {

    private ArrayList<Livro> acervo;

    public Acervo() {
        this.acervo = new ArrayList<Livro> ();
    }

    public boolean cadastrarLivro(int cod, String tit, String autor, String editora, int ano, int total) {
        acervo.add(new Livro(cod, tit, autor, editora, ano, total));
        return true;
    }

    public void listarTodos() {
        for (Livro livro : this.acervo) {
            System.out.println(livro);
        }
    }

    public void listarDisponiveis() {
        System.out.println("Livros disponíveis para o empréstimo: ");
        for (Livro livro : this.acervo) {
            if (livro.getTotal() > 0) {
                System.out.println(livro + " Total disponíveis: " + livro.getTotal());
            }
        }
    }
    public void ordenaCodigo() {
        acervo.sort(Comparator.comparing(Livro::getCodigo));
    }

    public void ordenaTitulo() {
        acervo.sort(Comparator.comparing(Livro::getTitulo).thenComparing(Livro::getAno));
    }

    public Livro buscarPorCodigo(int codigo) {

        for (Livro livro : this.acervo) {
            if (livro.getCodigo() == codigo){
                return livro;
            }
        }

        return null;
    }

    public Livro buscarPorTitulo(String titulo) {

        for (Livro livro : this.acervo) {
            if (livro.getTitulo().equals(titulo)){
                return livro;
            }
        }

        return null;
    }

    public Livro buscarPorAutor(String autor) {

        for (Livro livro : this.acervo) {
            if (livro.getAutor().equals(autor)){
                return livro;
            }
        }

        return null;
    }
}
