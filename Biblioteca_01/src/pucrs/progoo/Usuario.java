package pucrs.progoo;

public class Usuario implements Comparable<Usuario>{

	private String codigo;
    private String nome;
    private String cpf;

    public Usuario(String codigo, String nome, String cpf) {
        this.codigo = codigo;
        this.nome = nome;
        this.cpf = cpf;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {

        this.codigo = codigo;
    }

    public String getNome() {

        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return this.nome + " " + this.cpf;
    }

    @Override
    public int compareTo(Usuario usr) {
        return getNome().compareTo(usr.getNome());
    }
}
