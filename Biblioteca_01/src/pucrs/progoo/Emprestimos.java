package pucrs.progoo;

import java.util.ArrayList;
import java.time.LocalDate;
import java.util.Comparator;

public class Emprestimos {

	private ArrayList<Emprestimo> emprestimos;

	public Emprestimos() {
		this.emprestimos =  new ArrayList<Emprestimo>();
	}

	public void criar(Usuario usuario, Livro livro, LocalDate dataLimite) {
		if (livro.getTotal() > 0) {
			this.emprestimos.add(new Emprestimo(usuario, livro, dataLimite));
			livro.setTotal(livro.getTotal() - 1);
		}
	}

	public void devolver(Usuario usuario, Livro livro, LocalDate dataDevolucao) {
			this.buscarPorUsuarioLivro(usuario, livro).setDataDevolucao(dataDevolucao);
			livro.setTotal(livro.getTotal() + 1);
	}

	public void listarTodos() {
		for (Emprestimo emprestimo : this.emprestimos) {
			System.out.println(emprestimo);
		}
	}

	public ArrayList<Emprestimo> buscarPorUsuario(Usuario usuario) {
		ArrayList<Emprestimo> emprestimos = new ArrayList<Emprestimo>();

		for (Emprestimo empr : this.emprestimos) {
			if (empr.getUsuario() == usuario){
				emprestimos.add(empr);
			}
		}

		return emprestimos;
	}

	public Emprestimo buscarPorUsuarioLivro(Usuario usuario, Livro livro) {
		for (Emprestimo empr : this.emprestimos) {
			if (empr.getUsuario() == usuario && empr.getLivro() == livro){
				return empr;
			}
		}
		return null;
	}

	public void ordenaDataLimite() {
		emprestimos.sort(Comparator.comparing(Emprestimo::getDataLimite));
	}

	public void ordenaNomeData() {
        emprestimos.sort(Comparator.comparing((Emprestimo empr) -> {
            	Usuario usr =empr.getUsuario();
            	return usr.getNome();
        	}).thenComparing(Emprestimo::getDataLimite));
	}
}
