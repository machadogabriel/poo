package pucrs.progoo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class main {

    public static void main(String [] args)
    {
        Acervo acervo = new Acervo();
        Emprestimos emprestimos = new Emprestimos();
        Cadastro cadastro = new Cadastro();
        Usuario usr;
        Livro livro;
        ArrayList<Emprestimo> empr;
        boolean menu =  true;

        acervo.cadastrarLivro(003, "A arte da Guerra", "Sun Tzu", "Pensamento", 1995, 3);
        acervo.cadastrarLivro(004, "Memórias Póstumas de Brás Cubas", "Machado de Assis", "Ática", 1998, 1);
        acervo.cadastrarLivro(001, "O Senhor dos Aneis", "J. R. R. Tolkien", "Martins Fontes", 2000, 5);
        acervo.cadastrarLivro(002, "A origem das espécies", "Charles Darwin", "Hemus", 2000, 2);

        cadastro.cadastrar("13408618-4", "Paulo", "111.111.111-11");
        cadastro.cadastrar("13104668-3", "Machado", "041.952.540-69");
        cadastro.cadastrar("13404668-3", "João", "041.752.540-69");
        cadastro.cadastrar("13408618-3", "Paulo", "041.999.570-69");
        cadastro.cadastrar("13104668-2", "Gabriel", "031.952.540-69");

        System.out.println("Digite o nome do seu usuário:");
        Scanner scn = new Scanner(System.in);
        String nome = scn.next();

        usr = cadastro.buscarNome(nome);
        livro = acervo.buscarPorTitulo("O Senhor dos Aneis");


        emprestimos.criar(usr, livro, LocalDate.parse("2016-10-01"));
        emprestimos.criar(usr, acervo.buscarPorTitulo("A arte da Guerra"), LocalDate.parse("2015-10-01"));
        emprestimos.criar(usr, acervo.buscarPorTitulo("A origem das espécies"), LocalDate.parse("2016-08-01"));
        emprestimos.devolver(usr, livro, LocalDate.parse("2016-04-04"));
        empr = emprestimos.buscarPorUsuario(usr);

        System.out.println("Livros emprestados para: " + usr);
        for (Emprestimo emp : empr){
            System.out.println(emp);
        }
        System.out.println();

        while(menu) {

            System.out.println("Para devolver um livro digite 1");
            if (empr.size() > 0) {
                System.out.println("Para retirar um livro digite 2");
            }
            System.out.println("Para mudar de usuário digite 3");
            System.out.println("Para sair digite 9");


            int opc = scn.nextInt();

            if (opc == 1) {
                System.out.println("Digite o código do livro");
                int cod = scn.nextInt();
                Livro lvr = acervo.buscarPorCodigo(cod);
                emprestimos.devolver(usr, lvr, LocalDate.parse("2016-04-04"));
            } else if (opc == 2) {
                acervo.listarDisponiveis();
                System.out.println("Digite o código do livro");
                int cod = scn.nextInt();
                Livro lvr = acervo.buscarPorCodigo(cod);
                emprestimos.criar(usr, lvr, LocalDate.parse("2016-08-01"));
            } else if (opc == 3) {
                System.out.println("Digite o nome do seu usuário:");
                String novoNome = scn.next();
                usr = cadastro.buscarNome(novoNome);
                System.out.println("Agora você está acessando os dados de: " + usr);
                System.out.println();
            } if (opc == 9) {
                menu = false;
            } else System.out.println("opção inválida");
            }
    }
}


