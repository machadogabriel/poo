package pucrs.progoo;

interface Emprestavel {
    boolean emprestar();
    boolean devolver();
}
