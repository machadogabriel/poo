package pucrs.progoo;

public class Obra implements Emprestavel, Comparable<Obra>{

    private int codigo;
    private String titulo;
    private int ano;
    private int total;
    private String autor;

    public Obra(int codigo, String titulo, int ano, int total, String autor) {
        this.codigo = codigo;
        this.titulo = titulo;
        this.ano = ano;
        this.total = total;
        this.autor = autor;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getAutor() {
        return this.autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public boolean emprestar() {
        return false;
    }

    public boolean devolver() {
        return false;
    }

    @Override
    public String toString() {
        return "Titulo: " + this.titulo + " Ano: " + this.ano + " Autor:" + this.autor;
    }

    @Override
    public int compareTo(Obra o) {
        return this.titulo.compareTo(o.titulo);
    }
}
