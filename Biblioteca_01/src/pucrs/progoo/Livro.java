package pucrs.progoo;

public class Livro extends Obra{

    private String editora;
	private int total;

    public Livro(int codigo, String titulo, String autor, String editora, int ano, int total) {
        super(codigo, titulo, ano, total, autor);
        this.editora =  editora;
    }

	@Override
	public boolean emprestar() {
		if (total > 0) {
			total--;
			return true;
		}

		return false;
	}

	@Override
	public boolean devolver() {
		total ++;
		return true;
	}

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

	@Override
	public String toString() {
		return "Titulo: " + this.getTitulo() + " Ano: " + this.getAno() + " Código: " + this.getCodigo();
	}
}
