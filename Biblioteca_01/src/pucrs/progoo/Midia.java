package pucrs.progoo;

public class Midia extends Obra{

    String genero;
    String idioma;
    boolean cores;

    public Midia(int codigo, String titulo, String autor, int ano, int total, String genero, String idioma, boolean cores) {
        super(codigo, titulo, ano, total, autor);
        this.genero = genero;
        this.idioma =  idioma;
        this.cores = cores;
    }

    @Override
    public boolean emprestar() {
        return false;
    }

    @Override
    public boolean devolver() {
        return false;
    }

    @Override
    public String toString() {

        return super.toString();
    }

    public void setCores(boolean cores) {
        this.cores = cores;
    }

    public boolean getCores(){
        return this.cores;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getGenero(){
        return this.genero;
    }
     public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getIdioma(){
        return this.idioma;
    }
}
