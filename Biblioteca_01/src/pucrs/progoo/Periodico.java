package pucrs.progoo;

public class Periodico implements Emprestavel{

    private int codigo;
    private String titulo;
    private String autor;
    private String editora;
    private int ano;
    private int volume;
    private int numero;

    public boolean emprestar() {
        return true;
    }

    public boolean devolver() {
        return true;
    }

    public Periodico(int codigo, String titulo, String autor, String editora, int ano, int volume) {
        this.codigo = codigo;
        this.titulo = titulo;
        this.autor = autor;
        this.editora = editora;
        this.ano = ano;
        this.volume = volume;
        this.numero = numero;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Titulo: " + this.titulo + " Ano: " + this.ano;
    }
}
